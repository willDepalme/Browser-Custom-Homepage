var app = new App();

window.onload = function(){
  app.init(config);

  checkConnection();
  setInterval(checkConnection, 5000);
  checkForUpdates();

  initSettingsPanel();
  initClickListeners();
}

function initSettingsPanel(){
  for(let key in config['style']){
    let inputStyle = document.getElementById("in-" + key);
    if(inputStyle){
      inputStyle.value = CssManager.getProperty(key);
      if(key == "background-img") inputStyle.value = config["style"][key];
      inputStyle.addEventListener("change",function(e){
        if(key == "background-img"){
          document.getElementById("content").style.backgroundImage = "url(" + inputStyle.value + ")";
        } else {
          CssManager.setProperty(key,inputStyle.value);
        }
        config["style"][key] = inputStyle.value;
      })
    }
  }
}

function checkConnection(){
  if(!navigator.onLine){
    document.getElementById("connection").classList.replace("fa-wifi", "fa-spinner");
  } else {
    document.getElementById("connection").classList.replace("fa-spinner", "fa-wifi");
  }
}

function checkForUpdates(){
  var urlApi = "https://gitlab.com/api/v4/projects/5907358/repository/tags";

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var tagList = JSON.parse(this.response);
      var lastVersion = { "name" : config["version"] };

      for(var i in tagList){
        var tag = tagList[i];
        if(parseFloat(tag['name']) > parseFloat(lastVersion['name'])) lastVersion = tag; 
      }

      if(parseFloat(lastVersion['name']) > parseFloat(config["version"])){
        document.getElementById("updates").classList.toggle("available");
        document.getElementById("updateVersion").innerHTML = lastVersion['name'];
        document.getElementById("updateMessage").innerHTML = lastVersion['release']['description'];
      }
    }
  };
  xhttp.open("GET", urlApi, true);
  xhttp.send();
}


/* ---- Event functions ---- */

function webSearch(){
  app.webSearch();
}

function displaySettingsJson(){
  document.getElementById("jsonconfig").value = '"style" : ' + JSON.stringify(config['style'],null,2);
}

function initClickListeners(){
  document.getElementById("help").addEventListener("click", function(){
    document.getElementById("helpDialog").className = "open";
  });
  document.getElementById("helpDialog").addEventListener("click", function(){
    document.getElementById("helpDialog").className = "";
  });

  document.getElementById("updates").addEventListener("click", function(){
    document.getElementById("updatesDialog").className = "open";
  });
  document.getElementById("updatesDialog").addEventListener("click", function(){
    document.getElementById("updatesDialog").className = "";
  });

  document.getElementById("settings").addEventListener("click", function(){
    document.getElementById("settingsDialog").classList.toggle("open");
    app.setKeyboardMode(false);
  });
  document.getElementById("settings-close-button").addEventListener("click", function(){
    document.getElementById("settingsDialog").classList.toggle("open");
  });
}
