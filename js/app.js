class App {

  constructor() {
    console.log("-- INIT --");
    this.map = { "1":1, "2":2, "3":3, "4":4 };
    this.keyboardModeKey = "Shift";
    this.keyboardMode = false;
    this.openLinksInNewTab = false;
    this.currentTab = 0;
    this.currentItemIndex = 0;
    this.tabs = [];
    this.config = {};
  }

  init(config){
    this.config = config;
    let loaded = this.loadConfig(config);

    if(loaded){
      this.buildTabs(config["tabs"]);
      this.buildSearch(config["search"]);
      this.buildClock();
      setInterval(this.buildClock,5000);
      this.focusOnSearchBar();
      this.initListeners();
    } else {
      this.displayConfigErrorMessage();
    }
  }


/* ------------------------------------------------------- */


  loadConfig(config) {
    if (typeof config == 'undefined') return false;

    this.loadStyle(config['style']);

    if(config['openLinksInNewTab'] && config['openLinksInNewTab'] == true){
      this.openLinksInNewTab = true;
    }

    if(config["keyboardModeKey"]){
      this.keyboardModeKey = config['keyboardModeKey'];
      document.getElementById("control-keyboard").innerHTML = this.keyboardModeKey;
    }

    if(config['controls'] && config['controls'].length == 4){
      this.map = {};
      let conf = config['controls'];
      for(let i = 0 ; i < conf.length ; i++){
        this.map[conf[i].toUpperCase()] = (i+1);
        document.getElementById("control-tab"+(i+1)).innerHTML = conf[i].toUpperCase();
      }
    }

    return true;
  }

  loadStyle(styles){
    for(let key in styles){
      let style = styles[key];
      if(style != ""){
        if(key == "background-img"){
          document.getElementById("content").style.backgroundImage = "url(" + style + ")";
        } else {
          CssManager.setProperty(key,style);
        }
      }
    }
  }


/* ------------------------------------------------------- */


  buildTabs(tabsConfig){
    for(let iTab = 1 ; iTab <= 4 ; iTab++){
      let tab = new Tab("tab"+iTab);
      let items = tabsConfig[iTab-1]["items"];
      for(let it in items){
        let item = items[it];
        tab.addItem(item,this.openLinksInNewTab,it);
      }
      this.tabs[iTab-1] = tab;
    }
  }

  buildSearch(searchConfig){
    document.getElementById('searchbarInput').value = "";
    document.getElementById('searchbarInput').placeholder = searchConfig["name"] + " Search";
  }

  buildClock(){
    let d = new Date();
    let hour = ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
    document.getElementById("hour").innerHTML = hour;
    let day = d.toDateString();
    document.getElementById("date").innerHTML = day;
  }


/* ------------------------------------------------------- */


  focusOnSearchBar(){
    document.getElementById('searchbarInput').focus();
  }

  webSearch(){
    let url = this.config['search']['pattern'];
    let search = encodeURIComponent(document.getElementById('searchbarInput').value);
    url = url.replace('%s', search);

    window.open(url);
  }

  displayConfigErrorMessage(){
    console.log("config file not found");
    document.getElementById("errorConfigDialog").style.display = "flex";
    document.getElementById("content").style.display = "none";
  }


  getTab(id){
    return this.tabs[id-1];
  }

  getCurrentTab(){
    return this.tabs[this.currentTab-1];
  }

  hideOpenedTabs(){
    for(let oi in this.tabs){
      let tab = this.tabs[oi];
      tab.hide();
    }
    document.activeElement.ownerDocument.activeElement.blur(); // Remove focus
  }


  setKeyboardMode(mode){
    this.keyboardMode = mode;
    if(!this.keyboardMode){
      this.hideOpenedTabs();
      document.getElementById("keyboardMode").classList.remove("show");
      document.activeElement.ownerDocument.activeElement.blur();
      this.focusOnSearchBar();
    }
    else{
      document.getElementById("keyboardMode").classList.add("show");
    }
  }

  navigateInItems(direction){
    let tab = this.getCurrentTab();
    let items = tab.getItems();
    this.currentItemIndex = this.mod(this.currentItemIndex+direction, parseInt(items.length));
    tab.focusItem(this.currentItemIndex);
  }


  /* -------------------------------------------- */


  initListeners(){
    var app = this;

    window.addEventListener("keydown", function(e){

      if (app.map[e.key.toUpperCase()] != undefined) {
        if (app.keyboardMode) {
          let id = app.map[e.key.toUpperCase()];
          let tab = app.getTab(id);
          if(tab){
            if(!tab.isOpened()){
              app.hideOpenedTabs();
              tab.open();
              app.currentItemIndex = 0;
              app.currentTab = id;
            } else {
              tab.hide();
              app.hideOpenedTabs();
            }
          }
        }
      }

      if(app.keyboardMode){
        if(e.key == "ArrowRight") {
          app.navigateInItems(1);
        } else if(e.key == "ArrowLeft"){
          app.navigateInItems(-1);
        }
      }
    });

    window.addEventListener("keyup", function(e){
        if(e.key == app.keyboardModeKey) {
          app.setKeyboardMode(!app.keyboardMode);
        }
    });
  }


  /* -------------------------------------------- */


  mod(n, m) {
    return ((n % m) + m) % m;
  }
}
