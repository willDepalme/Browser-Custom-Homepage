class Tab {
  constructor(name) {
    this.name = name;
    this.dom = document.getElementById(this.name);
    this.itemsDom = [];
  }

  addItem(itemJson, openLinksInNewTab,id){
    /* Image Element */
    let itemImgDom = document.createElement("img");
    if(itemJson["iconPath"]) {
      itemImgDom.src = itemJson["iconPath"];
    } else {
      itemImgDom.src = itemJson["href"] + "/favicon.ico";
    }
    itemImgDom.alt = itemJson["name"];
    itemImgDom.title = itemJson["name"];

    /* Link Element */
    let itemLinkDom = document.createElement("a");
    if(openLinksInNewTab) itemLinkDom.target ="_blank";
    itemLinkDom.href = itemJson["href"];
    itemLinkDom.id = this.name + "-item" + id;
    itemLinkDom.className = "item";
    itemLinkDom.appendChild(itemImgDom);

    this.dom.getElementsByClassName("content-items")[0].appendChild(itemLinkDom);
    this.itemsDom.push(itemLinkDom);
  }

  hide(){
    this.dom.classList.remove("open")
  }

  open(){
    this.dom.classList.add("open");
    this.dom.getElementsByClassName("item")[0].focus();
  }

  isOpened(){
    return this.dom.classList.contains("open");
  }

  getItems(){
    return this.itemsDom;
  }

  focusItem(id){
    this.dom.getElementsByClassName("item")[id].focus();
  }
}
