class CssManager {
  constructor() {

  }

  static setProperty(property, style){
    document.documentElement.style.setProperty("--" + property,style);
  }

  static getProperty(property){
    return document.documentElement.style.getPropertyValue("--"+property);
  }
}
