var config = {
  "welcome_msg": "Home",
  "version": "1.0",
  "style": {
    "background-color" : "#333333",
    "background-img" : "",
    "quarter-color" : "#7ca2fe",
    "quarter-color-hover" : "#ffffff",
    "search-circle-bg": "#ffffff",
    "search-circle-color": "#333333"
  },
  "search": {
    "name": "DuckDuckGo",
    "pattern" :"https://duckduckgo.com/?q=%s"
   },
  "controls": "AZQS",
  "keyboardModeKey" : "Shift",
  "openLinksInNewTab" : false,
  "tabs": [
    {
      "items" : [
        {
          "name": "Facebook",
          "href" : "https://facebook.com",
          "iconPath": "icons/facebook.png"
        },
        {
          "name": "Twitter",
          "href" : "https://twitter.com",
          "iconPath": "icons/twitter.png"
        },
        {
          "name": "Reddit",
          "href" : "https://reddit.com",
          "iconPath": "icons/reddit.png"
        },
        {
          "name": "Imgur",
          "href" : "https://imgur.com",
          "iconPath": "icons/imgur.png"
        }
      ]
    },
    {
      "items" : [
        {
          "name": "Stack Overflow",
          "href" : "https://stackoverflow.com",
          "iconPath": "icons/stackoverflow.png"
        },
        {
          "name": "W3 School",
          "href" : "https://www.w3schools.com",
          "iconPath": "icons/w3school.ico"
        },
        {
          "name": "Open Classroom",
          "href" : "https://openclassrooms.com/",
          "iconPath": "icons/openclassroom.ico"
        },
        {
          "name": "DevDocs",
          "href" : "https://devdocs.io/",
          "iconPath": "https://devdocs.io/favicon.ico"
        }
      ]
    },
    {
      "items" : [
        {
          "name": "Youtube",
          "href" : "https://youtube.com",
          "iconPath": "icons/youtube.png"
        },
        {
          "name": "Dailymotion",
          "href" : "https://Dailymotion.com",
          "iconPath": "icons/dailymotion.png"
        },
        {
          "name": "LinkedIn",
          "href" : "https://linkedin.com",
          "iconPath": "icons/linkedin.png"
        },
        {
          "name": "Telegram",
          "href" : "https://web.telegram.org",
          "iconPath": "icons/telegram.png"
        }
      ]
    },
    {
      "items" : [
        {
          "name": "Github",
          "href" : "https://github.com",
          "iconPath": "https://github.com/favicon.ico"
        },
        {
          "name": "GitLab",
          "href" : "https://gitlab.com/",
          "iconPath": "https://gitlab.com/favicon.ico"
        },
        {
          "name": "Trello",
          "href" : "https://trello.com/",
          "iconPath": "https://trello.com/favicon.ico"
        },
        {
          "name": "Slack",
          "href" : "https://slack.com",
          "iconPath": "https://slack.com/favicon.ico"
        }
      ]
    }
  ]
};
