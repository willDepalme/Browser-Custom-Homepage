# Browser custom Home page

Access your favorite sites faster and with style ;)

![Thumbnail](demo.gif)

You can easily customize your bookmarks and style thanks to a simple JSON configuration file

![Thumbnail](screenshot_custom.png)

## Installation

### Create your configuration file

Copy the `conf.default.js` file and rename the copy `conf.js`
In the `conf.js` file you can edit your own preferences (colors, links, searchbar,...)

### Set as your default browser home page

Launch your favorite browser and set the home.html file as your home page.
- **Firefox** : Preferences -> Home page -> set path to the home.html file
- **Chrome, Chromium and Opera** : Settings -> On startup -> Select "Open a specific page" -> set path to the home.html file

## Customization

The `conf.js` file is a simple as possible JSON file where you can set your preferences.

### Style

The `style` section of the file represents values that will be used as CSS variables.

You can use every color format css offers :
- `#ffffff`
- `#ffffffff`
- `rgb(255,255,255)`
- and so on...

### Searchbar

In this section you can configure the search engine used in your search bar.
To set your own search engine do as follows :
1. Tap anything in your SE and launch the saarch
2. In the URL, look for the words you taped (may be encoded)
3. Replace your search by `%s`
4. Copy/Paste the new url in the pattern value of the configuration.

*ex*: Search "blue bird" in google

 	result = `https://www.google.com/search?q=blue+bird`  
 	The query is "blue+bird".  
 	The new URL will be : `https://www.google.com/search?q=%s`  

### Bookmarks

The `tabs` section is composed of 4 `items` sub-section which represent the links in the four quarters of the page.
In each `items` section you can add as many links as you wish but the preferred number of links is 4.

A link is composed of 3 values :
```
{
    "name": "Title of the wab page",
    "href" : "URL of the webpage",
    "iconPath": "path to page icon" (if not set, will use the default favicon of the webpage)
}	
```

### Live settings

This magnificent home page offers you a tool to test directly your color changes without reloading your page.

Click on the "Settings" button to open the settings modal and edit your preferences.

Once satisfied with what you get, click on the "Get config" button and copy the content display. Paste it to replace the old `style` section in your `conf.js` file. Reload your page the changes should appear.


Enjoy !
